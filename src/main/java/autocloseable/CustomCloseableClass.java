package autocloseable;

public class CustomCloseableClass implements AutoCloseable {

    @Override
    public void close() throws Exception {
        throw new Exception();
    }
}
