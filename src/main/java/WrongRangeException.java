public class WrongRangeException extends Exception {
    public WrongRangeException(String errorMessage) {
        super(errorMessage);
    }
}
