import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <strong>Task01_Fibonacci</strong>
 * This class Task01 consists of printEvenNumbers, printOddNumbers, printSumOfOddAndEvenNumb,
 * findFibonacciNumbers methods
 *
 * @author Iryna Voronovska
 * @version 1.0 26/08/19
 */
public class Task01 {
    private static final Logger LOG = LogManager.getLogger(Task01.class);

    /**
     * This is the main method that invokes all other methods from Task01
     *
     * @param args command line value
     */
    public static void main(String[] args) {
        try {
            int start = Integer.parseInt(scanFromConsole("Input start of the interval"));
            int end = Integer.parseInt(scanFromConsole("Input end of the interval"));
            int index = Integer.parseInt(scanFromConsole("Input size of set (index >=2 )"));
            checkRange(start, end);
            printEvenNumbers(start, end);
            printOddNumbers(start, end);
            printSumOfOddAndEvenNumb(start, end);
            findFibonacciNumbers(index);
        } catch (WrongRangeException e) {
            LOG.error(e);
            e.printStackTrace();
        }

    }

    private static String scanFromConsole(String message) {
        Scanner scan = new Scanner(System.in);
        System.out.println(message);
        return scan.nextLine();
    }

    private static void checkRange(int start, int end) throws WrongRangeException {
        if (start > end) {
            throw new WrongRangeException("Wrong! Start value should be less than end");
        }
    }

    /**
     * Method prints the even numbers from the interval that user inputted
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    private static void printEvenNumbers(int start, int end) {
        System.out.print("Even numbers ");
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println(" ");

    }

    /**
     * Method prints the odd numbers from the interval that user inputted
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    private static void printOddNumbers(int start, int end) {
        System.out.print("Odd numbers ");
        for (int i = end; i >= start; i--) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
        System.out.println(" ");
    }

    /**
     * Method prints sum of the odd and even numbers
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    private static void printSumOfOddAndEvenNumb(int start, int end) {
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                sumEven += i;
            } else {
                sumOdd += i;
            }
        }
        System.out.printf("Sum of odd numbers %d\nSum of even numbers %d\n", sumOdd, sumEven);
    }

    /**
     * Method find the Fibonacci numbers and print the biggest odd and even numbers from Fibonacci set.
     * Also print percentage of odd and even numbers
     *
     * @param index the size of Fibonacci numbers that user inputted
     */
    private static void findFibonacciNumbers(int index) {
        List<Integer> fibo = new ArrayList<>();
        fibo.add(0);
        fibo.add(1);
        int f1 = fibo.get(0);
        int f2 = fibo.get(1);
        int oddNumb = 1;
        int evenNumb = 1;
        while (fibo.size() < index) {
            int nextDigit = fibo.get(fibo.size() - 2) + fibo.get(fibo.size() - 1);
            fibo.add(nextDigit);
            if (nextDigit % 2 == 0) {
                f2 = nextDigit;
                evenNumb++;
            } else {
                f1 = nextDigit;
                oddNumb++;
            }
        }
        System.out.printf("The biggest odd number %d \nThe biggest even number %d\n", f1, f2);
        double percentageOddNumb = (double) (oddNumb * 100) / index;
        double percentageEvenNumb = 100 - percentageOddNumb;
        System.out.printf("Percentage of even numbers %.2f\n" +
                "Percentage of odd numbers %.2f", percentageEvenNumb, percentageOddNumb);
    }
}
